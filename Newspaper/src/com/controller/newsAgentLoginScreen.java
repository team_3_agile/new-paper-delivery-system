package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import com.dao.newsAgentLoginDAO;
import com.dao.newsAgentRegisterDAO;
import com.modal.newsAgentLoginImpl;
import com.modal.newsAgentRegistrationValidateImpl;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class newsAgentLoginScreen {

	private JFrame frame;
	private JTextField usernameTxt;
	private JPasswordField pswdTxt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					newsAgentLoginScreen window = new newsAgentLoginScreen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public newsAgentLoginScreen() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 507, 398);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("");
		label.setBounds(206, 214, 46, 13);
		frame.getContentPane().add(label);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblUsername.setBounds(99, 117, 75, 13);
		frame.getContentPane().add(lblUsername);
		
		usernameTxt = new JTextField();
		usernameTxt.setBounds(224, 115, 148, 19);
		frame.getContentPane().add(usernameTxt);
		usernameTxt.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Password");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(99, 171, 75, 13);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Newspaper Delivery System");
		lblNewLabel_1.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblNewLabel_1.setBounds(106, 28, 279, 33);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel errorLabel = new JLabel("");
		errorLabel.setBounds(125, 288, 247, 13);
		frame.getContentPane().add(errorLabel);
		
		newsAgentLoginImpl validate1 = new newsAgentLoginImpl();
		newsAgentLoginDAO dao1 = new newsAgentLoginDAO();
		
		JButton loginButton = new JButton("Login");
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(validate1.validateUsername(usernameTxt.getText()).equals("Please enter correct username!")) {
					errorLabel.setText("Please enter correct username!");
				}
				else if(validate1.validatePassword(pswdTxt.getText()).equals("Please enter correct password!")) {
					errorLabel.setText("Please enter correct password!");
				}
				else {
					if(dao1.Login(usernameTxt.getText(), pswdTxt.getText())) {
						errorLabel.setText("Login Successful");
						frame.setVisible(false);
						new newsAgent_mainScreen();
					}
					else {
						errorLabel.setText("Username or Password does not match");
					}
				}
			}
		});
		loginButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		loginButton.setBounds(124, 238, 85, 21);
		frame.getContentPane().add(loginButton);
		
		pswdTxt = new JPasswordField();
		pswdTxt.setBounds(224, 169, 148, 19);
		frame.getContentPane().add(pswdTxt);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//TODO: Go to register page
				frame.setVisible(false);
				new newsAgentRegistrationScreen();
			}
		});
		btnRegister.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnRegister.setBounds(243, 237, 110, 23);
		frame.getContentPane().add(btnRegister);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new firstScreen();
			}
		});
		btnBack.setBounds(392, 0, 99, 23);
		frame.getContentPane().add(btnBack);
		
		
	}
}
