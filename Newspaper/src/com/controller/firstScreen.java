package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class firstScreen {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					firstScreen window = new firstScreen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public firstScreen() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 523, 380);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewsPaperDelivery = new JLabel("News Paper Delivery System");
		lblNewsPaperDelivery.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblNewsPaperDelivery.setBounds(122, 28, 279, 36);
		frame.getContentPane().add(lblNewsPaperDelivery);
		
		JButton btnNewsAgent = new JButton("News Agent ?");
		btnNewsAgent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				new newsAgentLoginScreen();
			}
		});
		btnNewsAgent.setFont(new Font("SansSerif", Font.BOLD, 14));
		btnNewsAgent.setBounds(49, 144, 153, 82);
		frame.getContentPane().add(btnNewsAgent);
		
		JButton btnNewButton = new JButton("Delivery Person?");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new deliveryPerson_Login();
			}
		});
		btnNewButton.setFont(new Font("SansSerif", Font.BOLD, 14));
		btnNewButton.setBounds(233, 144, 223, 82);
		frame.getContentPane().add(btnNewButton);
	}
}
