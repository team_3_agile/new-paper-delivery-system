package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import com.dao.deliveryPersonLoginDAO;
import com.modal.deliveryPersonLoginImpl;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class deliveryPerson_Login {

	private JFrame frame;
	private JTextField Uname;
	private JPasswordField Pass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					deliveryPerson_Login window = new deliveryPerson_Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public deliveryPerson_Login() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 509, 441);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDeliveryPersonLogin = new JLabel("Delivery Person Login");
		lblDeliveryPersonLogin.setFont(new Font("SansSerif", Font.BOLD, 17));
		lblDeliveryPersonLogin.setBounds(150, 27, 196, 26);
		frame.getContentPane().add(lblDeliveryPersonLogin);
		
		JLabel lblUsername = new JLabel("Username: ");
		lblUsername.setBounds(113, 125, 67, 14);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password: ");
		lblPassword.setBounds(113, 202, 67, 14);
		frame.getContentPane().add(lblPassword);
		
		Uname = new JTextField();
		Uname.setBounds(237, 122, 148, 20);
		frame.getContentPane().add(Uname);
		Uname.setColumns(10);
		
		Pass = new JPasswordField();
		Pass.setBounds(237, 199, 148, 20);
		frame.getContentPane().add(Pass);
		
		JLabel errLbl = new JLabel("");
		errLbl.setBounds(129, 352, 240, 14);
		frame.getContentPane().add(errLbl);
		
		deliveryPersonLoginImpl delVal = new deliveryPersonLoginImpl();
		deliveryPersonLoginDAO delDAO = new deliveryPersonLoginDAO();
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//TODO: Production Code
				if(delVal.validateUsername(Uname.getText()).equals("Please enter correct username!")) {
					errLbl.setText("Please enter correct username!");
				}
				else if(delVal.validatePassword(Pass.getText()).equals("Please enter correct password!")) {
					errLbl.setText("Please enter correct password!");
				}
				else {
					if(delDAO.Login(Uname.getText(), Pass.getText())) {
						errLbl.setText("Login Successful");
						frame.setVisible(false);
						new deliveryPerson_mainScreen();
						
					}
					else {
						errLbl.setText("Username or Password does not match");
					}
				}
				
			}
		});
		btnLogin.setBounds(191, 292, 89, 23);
		frame.getContentPane().add(btnLogin);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new firstScreen();
			}
		});
		btnBack.setBounds(404, 0, 89, 23);
		frame.getContentPane().add(btnBack);
		
		
	}
}
