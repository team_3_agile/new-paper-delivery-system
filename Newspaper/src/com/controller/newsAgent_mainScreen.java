package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class newsAgent_mainScreen {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					newsAgent_mainScreen window = new newsAgent_mainScreen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public newsAgent_mainScreen() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 595, 446);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewsPaperDelivery = new JLabel("News Paper Delivery System");
		lblNewsPaperDelivery.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblNewsPaperDelivery.setBounds(159, 45, 273, 37);
		frame.getContentPane().add(lblNewsPaperDelivery);
		
		JButton btnNewButton = new JButton("Customer");
		btnNewButton.setBounds(70, 170, 127, 45);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnDeliveryPerson = new JButton("Delivery Person");
		btnDeliveryPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new DeliveryPersonRegister();
			}
		});
		btnDeliveryPerson.setBounds(226, 170, 127, 45);
		frame.getContentPane().add(btnDeliveryPerson);
		
		JButton btnSubscription = new JButton("Subscription");
		btnSubscription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new subscription_menu_Screen();
			}
		});
		btnSubscription.setBounds(389, 170, 127, 45);
		frame.getContentPane().add(btnSubscription);
		
		JButton btnOrder = new JButton("Order");
		btnOrder.setBounds(70, 267, 127, 45);
		frame.getContentPane().add(btnOrder);
		
		JButton btnDeliveryDocket = new JButton("Delivery Docket");
		btnDeliveryDocket.setBounds(226, 267, 127, 45);
		frame.getContentPane().add(btnDeliveryDocket);
		
		JButton btnInvoiceStock = new JButton("Invoice & Stock");
		btnInvoiceStock.setBounds(389, 267, 127, 45);
		frame.getContentPane().add(btnInvoiceStock);
		
		JButton btnLogOut = new JButton("Log Out");
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				new newsAgentLoginScreen();
			}
		});
		btnLogOut.setBounds(486, 0, 93, 23);
		frame.getContentPane().add(btnLogOut);
		
		JLabel lblNewsAgent = new JLabel("News Agent");
		lblNewsAgent.setFont(new Font("SansSerif", Font.BOLD, 13));
		lblNewsAgent.setBounds(246, 93, 113, 23);
		frame.getContentPane().add(lblNewsAgent);
	}
}
