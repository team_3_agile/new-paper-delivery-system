package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class deliveryPerson_mainScreen {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					deliveryPerson_mainScreen window = new deliveryPerson_mainScreen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public deliveryPerson_mainScreen() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 550, 397);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("News Paper Delivery System");
		label.setFont(new Font("SansSerif", Font.BOLD, 18));
		label.setBounds(133, 32, 273, 37);
		frame.getContentPane().add(label);
		
		JLabel lblDeliveryPerson = new JLabel("Delivery Person");
		lblDeliveryPerson.setFont(new Font("SansSerif", Font.BOLD, 13));
		lblDeliveryPerson.setBounds(209, 92, 113, 31);
		frame.getContentPane().add(lblDeliveryPerson);
		
		JButton btnNewButton = new JButton("Log Out");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				new deliveryPerson_Login();
			}
		});
		btnNewButton.setFont(new Font("SansSerif", Font.BOLD, 14));
		btnNewButton.setBounds(95, 199, 347, 54);
		frame.getContentPane().add(btnNewButton);
	}
}
