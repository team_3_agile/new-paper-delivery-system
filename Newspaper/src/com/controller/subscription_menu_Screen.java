package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class subscription_menu_Screen {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					subscription_menu_Screen window = new subscription_menu_Screen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public subscription_menu_Screen() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 536, 419);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSubscription = new JLabel("Subscription");
		lblSubscription.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblSubscription.setBounds(197, 27, 122, 35);
		frame.getContentPane().add(lblSubscription);
		
		JButton btnCreateSubscription = new JButton("Create Subscription");
		btnCreateSubscription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new createSubscriptionScreen();
			}
		});
		btnCreateSubscription.setBounds(66, 132, 165, 45);
		frame.getContentPane().add(btnCreateSubscription);
		
		JButton btnReadSubscription = new JButton("Read Subscription");
		btnReadSubscription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new readSubscriptionScreen();
			}
		});
		btnReadSubscription.setBounds(279, 132, 165, 45);
		frame.getContentPane().add(btnReadSubscription);
		
		JButton btnDeleteSubscription = new JButton("Delete Subscription");
		btnDeleteSubscription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new SubscriptionDeleteScreen();
			}
		});
		btnDeleteSubscription.setBounds(66, 243, 165, 45);
		frame.getContentPane().add(btnDeleteSubscription);
		
		JButton btnUpdateSubscription = new JButton("Update Subscription");
		btnUpdateSubscription.setBounds(279, 243, 165, 45);
		frame.getContentPane().add(btnUpdateSubscription);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				new newsAgent_mainScreen();
			}
		});
		btnBack.setBounds(423, 0, 97, 23);
		frame.getContentPane().add(btnBack);
	}
}
