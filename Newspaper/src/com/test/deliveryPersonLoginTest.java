package com.test;

import com.modal.deliveryPersonLoginImpl;

import junit.framework.TestCase;

public class deliveryPersonLoginTest extends TestCase {

	deliveryPersonLoginImpl deliP = new deliveryPersonLoginImpl();
	/*
	Test Number	1
	Test Objective	Valid username (length >=3)
	Test Type	jUnit
	Input (s)	UN = abc 
	Expected Output	Correct Username
	 */
	public void testValidUsername() {
		assertEquals("Correct Username!", deliP.validateUsername("abcd"));
	}
	
	/*
 	Test Number:  2
	Test Objective: Valid username (length <=50)
	Test Type:	jUnit
	Input (s):	UN = abcdefghijklmn
	Expected Output:	Correct Username
 	*/
	public void testValidUsername2() {
		assertEquals("Correct Username!", deliP.validateUsername("abcdefghijklmn"));
	}
	
	/*
 	Test Number: 3
	Test Objective: InValid username (length <3)
	Test Type	jUnit
	Input (s)	UN = ab
	Expected Output	Incorrect Username
 	*/
	public void testInValidUsername() {
		assertEquals("Please enter correct username!", deliP.validateUsername("ab"));
	}
	
	/*
 	Test Number 4
	Test Objective InValid username (length >50)
	Test Type	jUnit
	Input (s)	UN = ab
	Expected Output	Incorrect Username
 	*/
	public void testInValidUsername2() {
		assertEquals("Please enter correct username!", deliP.validateUsername("aqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm"));
	}

	/*
 	Test Number	5
	Test Objective	Valid Password (length >=8)
	Test Type	jUnit
	Input (s)	Pswd = abc@12345
	Expected Output	Correct Password
 	*/
	public void testValidPassword() {
		assertEquals("Correct password!", deliP.validatePassword("abc@12345"));
	}
	
	/*
 	Test Number	6
	Test Objective	Valid Password (length <=20)
	Test Type	jUnit
	Input (s)	Pswd = qwertyuiopasdfghjkl
	Expected Output	Correct Password
 	*/
	public void testValidPassword2() {
		assertEquals("Correct password!", deliP.validatePassword("qwertyuiopasdfghjkl"));
	}
	
	/*
 	Test Number	7
	Test Objective	InValid Password (length <8)
	Test Type	jUnit
	Input (s)	Pswd = abc@12
	Expected Output	Incorrect Password
 	*/
	public void testInValidPassword() {
		assertEquals("Please enter correct password!", deliP.validatePassword("abcdef"));
	}
	
	/*
 	Test Number	8
	Test Objective	InValid Password (length >20)
	Test Type	jUnit
	Input (s)	Pswd = abc@12
	Expected Output	Incorrect Password
 	*/
	public void testInValidPassword2() {
		assertEquals("Please enter correct password!", deliP.validatePassword("qwertyuiopasdfghjklzxcv"));
	}

}
